﻿using NUnit.Framework;
using System;
using cashier;
using System.Collections.Generic;
using cashier.Domain;
using cashier.Data.Models;
using cashier.Domain.BusinessLogic;

namespace tests
{
    [TestFixture()]
    public class Test
    {
        [Test()]
        public void CurrenciesTest()
        {
            //arrange
            List<Currency> currencyList = new List<Currency>();
            var xmlReader = new XmlReader();
            Assert.IsTrue(currencyList != null);

            //act
            currencyList = ReturnCurrencies.GetCurrencies(xmlReader);

            //assert
            Assert.IsTrue(currencyList.Count > 0);
        }
    }
}
