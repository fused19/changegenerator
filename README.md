# Hello World #

Clone and build the project. After building and running successfully, have a look in the project folder and you should find a new folder 'log' with text files in it also there should be another new folder 'json' with json output files in it. These are the output from the app.

# The Project: Change Calculator #

A change calculator is required that handles GBP, EUR and USD currencies.    We need to take as inputs a presented amount and a product price and the change returned must be calculated in the most efficient manner, i.e. by highest to lowest denominator.
 
Given a presented amount of £200 and a product price of £49.21; the primary output should look like the following:
 
“You supplied £200, with a product price of £49.21.
 
This is your change:
3 x £50
1 x 50p
1 x 20p
1 x 5p
2 x 2p
 
Total change: £150.79p
 
There is a need as a secondary type of output for a forthcoming feature that returns a JSON representation of the change.
