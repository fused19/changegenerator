﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using System.Text;
using cashier.Data.Models;
using cashier.Domain.BusinessLogic;
using cashier.Interfaces;
using cashier.Models;
using NLog;

namespace cashier
{
    public class FileHandle
    {
        private readonly IXmlReader XmlReader;
        private readonly IWriter Writer;
        private readonly ICashier Cashier;
        private readonly Logger logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType.ToString());
        private readonly string sessionId;
        private readonly double tenderedValue = double.Parse(ConfigurationManager.AppSettings["tenderedValue"]);
        private readonly double productValue = double.Parse(ConfigurationManager.AppSettings["productValue"]);
        private Dictionary<Currency, int> changeRequired = new Dictionary<Currency, int>();

        public FileHandle(Dependencies dependencies)
        {
            sessionId = GetSessionId();
            XmlReader = dependencies.XmlReader;
            Writer = dependencies.Writer;
            Cashier = dependencies.Cashier;
        }

        private static string GetSessionId()
        {
            DateTime date = DateTime.Now;
            return date.ToString("yyyyMMdd");
        }

        public void Run()
        {
            try
            {
                Start();
                Main();
                End();

            }
            catch (Exception generalException)
            {
                logger.Error($"Session Id ({sessionId}) => Failed for the following reason: {generalException.Message}.\n STACK TRACE\n {generalException.StackTrace}");
            }
        }

        private void Start()
        {
            LogManager.ThrowExceptions = true;
            logger.Info("****************************************************************************");
            logger.Info($"Session Id ({sessionId}) => Starting.");
        }

        private void End()
        {
            logger.Info($"Session Id ({sessionId}) => Finished.");
            logger.Info("****************************************************************************");
        }

        private void Main()
        {
            IEnumerable<Currency> change = Cashier.Calculate(tenderedValue, productValue, ReturnCurrencies.GetCurrencies(XmlReader));

            CurrencyDescription description = ReturnCurrencies.GetCurrencyDetails(XmlReader).Descriptor;

            foreach (Currency thisValue in change)
            {
                if (changeRequired.ContainsKey(thisValue))
                {
                    changeRequired[thisValue]++;
                }
                else
                {
                    changeRequired.Add(thisValue, 1);
                }
            }
            OutPutMsg(change, description);
        }

        private void OutPutMsg(IEnumerable<Currency> change, CurrencyDescription description)
        {
            double ChangeTotal = Cashier.ConvertToTotalAmount(change);

            var text = new StringBuilder("This is the change broken down: " + Environment.NewLine);

            foreach (KeyValuePair<Currency, int> value in changeRequired)
            {
                text.Append(value.Value + " of " + value.Key.Name.ToString() + Environment.NewLine);
            }
            logger.Info($"Session Id ({sessionId}) => change generated: {description.Country}.");
            logger.Info($"Session Id ({sessionId}) => change generated: {description.CurrencyID}.");
            logger.Info($"Session Id ({sessionId}) => change generated: {description.DescriptionName}.");
            logger.Info("********************************************************************************");
            logger.Info($"Session Id ({sessionId}) => item cost: {productValue}.");
            logger.Info($"Session Id ({sessionId}) => money presented: {tenderedValue}.");
            logger.Info($"Session Id ({sessionId}) => change total: {ChangeTotal}.");
            logger.Info($"Session Id ({sessionId}) => change generated: {text}.");
            logger.Info("********************************************************************************");
            writeToFile(changeRequired, ChangeTotal);
        }

        private void writeToFile(Dictionary<Currency, int> changeRequired, double grandTotal)
        {
            Change changeObj = new Change();

            foreach (var item in changeRequired)
            {
                changeObj.ChangeItem.Add(item.Value + " of " + item.Key.Name.ToString());
            }
            changeObj.Total = grandTotal;

            Writer.WriteFile<Change>(changeObj);
        }
    }
}
