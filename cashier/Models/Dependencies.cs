﻿using System;
using cashier.Interfaces;

namespace cashier.Models
{
    public class Dependencies
    {
        public IXmlReader XmlReader { get; private set; }
        public IWriter Writer { get; private set; }
        public ICashier Cashier { get; private set; }

        public Dependencies(IXmlReader xmlReader, IWriter writer, ICashier cashier)
        {
            this.XmlReader = xmlReader;
            this.Writer = writer;
            this.Cashier = cashier;
        }
    }
}
