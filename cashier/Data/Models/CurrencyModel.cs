﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;

namespace cashier.Data.Models
{
    [XmlRoot("Currencies")]
    public class Currencies
    {

        [XmlElement("Currency")]
        public List<Currency> Items { get; set; }

        [XmlElement("CurrencyDescription")]
        public CurrencyDescription Descriptor { get; set; }

        public Currencies() { Items = new List<Currency>(); Descriptor = new CurrencyDescription(); }

        private PropertyInfo[] _PropertyInfos = null;

        public override string ToString()
        {
            if (_PropertyInfos == null)
                _PropertyInfos = this.GetType().GetProperties();

            var sb = new StringBuilder();

            foreach (var info in _PropertyInfos)
            {
                var value = info.GetValue(this, null) ?? "(null)";
                sb.AppendLine(info.Name + ": " + value.ToString());
            }

            return sb.ToString();
        }
    }

    [XmlRoot("Currency")]
    public class Currency : IComparable<Currency>
    {

        [XmlElement("CurrencyName")]
        public string Name { get; set; }

        [XmlElement("CurrencyValue")]
        public int Value { get; set; }

        private PropertyInfo[] _PropertyInfos = null;

        public override string ToString()
        {
            if (_PropertyInfos == null)
                _PropertyInfos = this.GetType().GetProperties();

            var sb = new StringBuilder();

            foreach (var info in _PropertyInfos)
            {
                var value = info.GetValue(this, null) ?? "(null)";
                sb.AppendLine(info.Name + ": " + value.ToString());
            }

            return sb.ToString();
        }

        public int CompareTo(Currency other)
        {
            return this.Value.CompareTo(other.Value);
        }

    }

    [XmlRoot("CurrencyDescription")]
    public class CurrencyDescription
    {
        [XmlElement("DescriptionName")]
        public string DescriptionName { get; set; }

        [XmlElement("CurrencyID")]
        public string CurrencyID { get; set; }

        [XmlElement("Country")]
        public string Country { get; set; }

        private PropertyInfo[] _PropertyInfos = null;

        public override string ToString()
        {
            if (_PropertyInfos == null)
                _PropertyInfos = this.GetType().GetProperties();

            var sb = new StringBuilder();

            foreach (var info in _PropertyInfos)
            {
                var value = info.GetValue(this, null) ?? "(null)";
                sb.AppendLine(info.Name + ": " + value.ToString());
            }

            return sb.ToString();
        }

    }
}
