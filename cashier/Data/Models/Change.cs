﻿using System;
using System.Collections.Generic;

namespace cashier.Data.Models
{
    public class Change
    {
        public string Transaction { get; private set; }
        public List<string> ChangeItem { get; set; }
        public double Total { get; set; }

        public Change()
        {
            this.ChangeItem = new List<string>();
            this.Transaction = Guid.NewGuid().ToString();
            this.Total = 0.0;
        }
    }
}
