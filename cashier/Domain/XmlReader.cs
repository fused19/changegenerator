﻿using System;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;
using cashier.Interfaces;

namespace cashier.Domain
{
    public class XmlReader : IXmlReader
    {
        public List<T> ReadListXml<T>(string path, string root)
        {
            List<T> ItemList = default(List<T>);
            if (string.IsNullOrEmpty(path)) return default(List<T>);

            XmlSerializer serializer = new XmlSerializer(typeof(List<T>), new XmlRootAttribute(root));
            using (StreamReader xmlStream = new StreamReader(path))
            {
                ItemList = (List<T>)serializer.Deserialize(xmlStream);
            }
            return ItemList;
        }

        public T ReadXml<T>(string path)
        {
            T returnObject = default(T);
            if (string.IsNullOrEmpty(path)) return default(T);

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                using (StreamReader xmlStream = new StreamReader(path))
                {
                    returnObject = (T)serializer.Deserialize(xmlStream);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return returnObject;
        }
    }
}
