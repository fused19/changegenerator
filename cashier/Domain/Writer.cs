﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using cashier.Interfaces;
using Newtonsoft.Json;

namespace cashier.Domain
{
    public class Writer : IWriter

    {
        private string filePath;

        public Writer(string filePath)
        {
            this.filePath = filePath;
        }

        public void WriteFile<T>(T items)
        {
            T Items = items;
            if (Items != null)
            {
                writeFile<T>(Items);
            }
        }

        private void writeFile<T>(T Items)
        {
            VerifyDirectory(filePath);

            string json = JsonConvert.SerializeObject(Items, Formatting.Indented);
            File.WriteAllText(filePath + $"file-{Guid.NewGuid()}.json", json);
        }

        private void VerifyDirectory(string filePath)
        {
            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);
        }
    }
}
