﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using cashier.Data.Models;
using cashier.Interfaces;

namespace cashier.Domain.BusinessLogic
{
    public class ReturnCurrencies
    {

        public static List<Currency> GetCurrencies(IXmlReader XmlReader)
        {
            var xmlReader = XmlReader;
            var currenciesPath = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, ConfigurationManager.AppSettings["TestFileName"]));
            var currencies = xmlReader.ReadListXml<Currency>(currenciesPath.ToString(), "Currencies");
            return currencies;
        }

        public static Currencies GetCurrencyDetails(IXmlReader XmlReader)
        {
            var xmlReader = XmlReader;
            var currencyPath = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, ConfigurationManager.AppSettings["TestFileName"]));
            var descriptions = xmlReader.ReadXml<Currencies>(currencyPath.ToString());
            return descriptions;
        }
    }
}
