﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using cashier.Interfaces;

namespace cashier.Domain
{
    public class Cashier : ICashier
    {


        public IEnumerable<T> Calculate<T>(double tendered, double price, IEnumerable<T> faceValueCurrency)
        {
            var CountEachOccurance = new List<T>();
            var tenderedValue = (int)(tendered * 100);
            var priceValue = (int)(price * 100);
            var change = tenderedValue - priceValue;

            foreach (T EachItem in faceValueCurrency)
            {
                var itemType = typeof(T);
                var value = (int)itemType.GetProperty("Value").GetValue(EachItem, null);
                var CountInstances = 0;

                CountInstances = (value <= change) ? (change / value) : CountInstances;

                for (var i = 0; i < CountInstances; i++)
                {
                    CountEachOccurance.Add(EachItem);
                }

                change -= (CountInstances * value);
            }
            return CountEachOccurance;
        }

        public double ConvertToTotalAmount<T>(IEnumerable<T> FaceValue)
        {
            double total = 0.00;

            foreach (T item in FaceValue)
            {
                var itemType = typeof(T);
                var value = (int)itemType.GetProperty("Value").GetValue(item, null);

                total += value;
            }
            return total / 100;
        }

    }
}
