﻿using System;
using System.Configuration;
using cashier.Domain;
using cashier.Interfaces;
using cashier.Models;

namespace cashier
{
    class MainClass
    {

        public static void Main(string[] args)
        {
            try
            {
                IXmlReader xmlDataReader = new XmlReader();
                IWriter writer = new Writer(ConfigurationManager.AppSettings["OutPutFile"]);
                ICashier cashier = new Cashier();

                Dependencies dependencies = new Dependencies(xmlDataReader, writer, cashier);

                new FileHandle(dependencies).Run();
            }
            catch (Exception ex)
            {
                Environment.Exit(1);
            }
        }
    }
}
