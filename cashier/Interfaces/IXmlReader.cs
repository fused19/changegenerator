﻿using System;
using System.Collections.Generic;

namespace cashier.Interfaces
{
    public interface IXmlReader
    {
        T ReadXml<T>(string path);

        List<T> ReadListXml<T>(string path, string root);
    }
}
