﻿using System;
using System.Collections.Generic;

namespace cashier.Interfaces
{
    public interface IWriter
    {
        void WriteFile<T>(T items);
    }
}
