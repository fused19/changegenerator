﻿using System.Collections.Generic;

namespace cashier.Interfaces
{
    public interface ICashier
    {

        IEnumerable<T> Calculate<T>(double tendered, double price, IEnumerable<T> faceValueCurrency);

        double ConvertToTotalAmount<T>(IEnumerable<T> FaceValue);
    }
}
